import requests
from bs4 import BeautifulSoup
import pandas as pd
import sys
import os


def connect(url, retry=10):
    times = 0
    while times < retry:
        try:
            times += 1
            soup = BeautifulSoup(requests.get(url).content, 'html.parser')
            return soup
        except:
            pass


host = 'https://www.drugs.com'
soup = connect('https://www.drugs.com/drug_information.html')

pages = soup.find('ul', {'class': 'ddc-paging'}).findAll('a')

train_scrapped = pd.DataFrame(
    columns=['name', 'condition', 'opinion', 'rate', 'rate1'])

for page in pages:
    page_href = page['href']
    page_soup = connect(host + page_href)

    subpages = page_soup.find('ul', {'class': 'ddc-paging'}).findAll('a')
    for subpage in subpages:
        subpage_href = subpage['href']
        subpage_soup = connect(host + subpage_href)

        try:
            subpage_drug_links = subpage_soup.select(
                'ul[class*="ddc-list"]')[0].findAll('a')
        except IndexError:
            continue

        for subpage_drug_link in subpage_drug_links:
            drug_soup = connect(host + subpage_drug_link['href'])
            try:
                drug_reviews_href = drug_soup.select(
                    'p.user-reviews-title a')[0]['href']
                drug_name = drug_soup.select(
                    'p.user-reviews-title a')[0].get_text().replace(" reviews", "")
            except IndexError:
                continue

            if drug_name in train_scrapped['name']:
                print(f"{drug_name} already scrapped")
                continue
            drug_reviews_soup = connect(host + drug_reviews_href)
            try:
                condition_rows = drug_reviews_soup.select(
                    'table[class*="ddc-table"]')[0].findAll('tr', recursive=False)
            except (IndexError, AttributeError):
                continue
            for condition_row in condition_rows:
                drug_condition = condition_row.find('th').get_text()
                reviews_href = condition_row.find('a')['href']
                comment_contents = []
                comment_ratings = []
                while True:
                    reviews_soup = connect(host + reviews_href)
                    if reviews_soup is None:
                        continue
                    comment_contents += [comment_content.find('span').get_text()
                                         for comment_content in reviews_soup.select('p.ddc-comment-content')]
                    comment_ratings += [comment_rating.get_text()
                                        for comment_rating in reviews_soup.select('div.comment-rating-score')]
                    next_page_link = reviews_soup.find(
                        'li', {'class': 'ddc-paging-item-next'})

                    if next_page_link is not None:
                        reviews_link = next_page_link.find('a')
                        if reviews_link is None:
                            break
                        else:
                            reviews_href = reviews_link['href']
                    else:
                        break
                for opinion, rate in zip(comment_contents, comment_ratings):
                    name = str(drug_name)
                    condition = str(drug_condition)
                    opinion = str(opinion)
                    rate = int(float(rate))
                    if rate < 4:
                        rate1 = "low"
                    elif rate > 6:
                        rate1 = "high"
                    else:
                        rate1 = "medium"
                    row = pd.Series({
                        'name': name,
                        'condition': condition,
                        'opinion': opinion,
                        'rate': rate,
                        'rate1': rate1
                    })
                    train_scrapped = train_scrapped.append(
                        row, ignore_index=True)
            train_scrapped.to_csv(
                f"{os.path.join(sys.path[0], 'train_scrapped.csv')}", index=False, sep=";")
            print("Updated!")
