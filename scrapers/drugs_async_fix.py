import requests
from bs4 import BeautifulSoup
import pandas as pd
import os
import sys
import time
from celery import Celery
import re

timestamp = int(time.time())
host = 'https://www.drugs.com'


app = Celery('drugs_async_fix',
             backend='rpc://',
             broker='pyamqp://guest@localhost//')


def connect(url, retry=10):
    times = 0
    while times < retry:
        try:
            times += 1
            response = requests.get(
                url, headers={'User-agent': 'your bot 0.1'})
            print(response.status_code)
            soup = BeautifulSoup(response.content, 'html.parser')
            return soup
        except requests.exceptions.ConnectionError:
            print("”błąd")


@app.task(rate_limit=10)
def get_opinions_from_page(url):
    page_soup = connect(url)
    subpage_tasks = []
    for a in page_soup.find('ul', {'class': 'ddc-paging'}).findAll('a'):
        get_opinions_from_subpage.delay(host+a['href'])


@app.task(rate_limit=10)
def get_opinions_from_subpage(url):
    subpage_soup = connect(url)
    drug_tasks = []
    try:
        subpage_drug_links = subpage_soup.select(
            'ul[class*="ddc-list"]')[0].findAll('a')
    except IndexError:
        return
    for a in subpage_drug_links:
        get_opinions_from_drug.delay(host+a['href'])


@app.task(rate_limit=10)
def get_opinions_from_drug(url):
    drug_soup = connect(url)
    try:
        drug_reviews_href = drug_soup.select(
            'p.user-reviews-title a')[0]['href']
        drug_name = drug_soup.select(
            'p.user-reviews-title a')[0].get_text().replace(" reviews", "")
    except IndexError:
        return
    drug_reviews_soup = connect(host+drug_reviews_href)
    try:
        condition_rows = drug_reviews_soup.select(
            'table[class*="ddc-table"]')[0].findAll('tr', recursive=False)
    except (IndexError, AttributeError):
        return
    condition_tasks = []
    for condition_row in condition_rows:
        drug_condition = condition_row.find('th').get_text()
        condition_href = condition_row.find('a')['href']
        try:
            reviews_count = int(condition_row.find('a')['href'].split(' ')[0])
        except:
            continue
        if reviews_count <= 25:
            get_opinions_from_reviews.delay(
                host+condition_href, drug_name, drug_condition)


@app.task(rate_limit=10, ignore_result=True)
def get_opinions_from_condition(drug_condition, condition_href, drug_name):
    comment_contents_all = []
    comment_ratings_all = []
    condition_soup = connect(host+condition_href)
    reviews_tasks = []
    try:
        for a in condition_soup.find('ul', {'class': 'ddc-paging'}).findAll('a'):
            result = get_opinions_from_reviews.delay(
                host+a['href'], drug_name, drug_condition)
    except AttributeError:
        return


@app.task(rate_limit=20)
def get_opinions_from_reviews(url, drug_name, drug_condition):
    train_scrapped = pd.DataFrame(
        columns=['name', 'condition', 'opinion', 'rate', 'rate1'])
    reviews_soup = connect(url)
    if reviews_soup is None:
        return
    comment_contents = [comment_content.find('span').get_text()
                        for comment_content in reviews_soup.select('p.ddc-comment-content')]
    comment_ratings = [comment_rating.get_text()
                       for comment_rating in reviews_soup.select('div.comment-rating-score')]
    for opinion, rate in zip(comment_contents, comment_ratings):
        name = str(drug_name)
        condition = str(drug_condition)
        opinion = str(opinion)
        rate = int(float(rate))
        if rate < 4:
            rate1 = "low"
        elif rate > 6:
            rate1 = "high"
        else:
            rate1 = "medium"
        row = pd.Series({
            'name': name,
            'condition': condition,
            'opinion': opinion,
            'rate': rate,
            'rate1': rate1
        })
        train_scrapped = train_scrapped.append(
            row, ignore_index=True)
    train_scrapped.drop_duplicates(subset=['condition', 'opinion'])
    train_scrapped.to_csv(
        '~/train_scrapped.csv', index=False, sep=";", mode="a")
    print("Updated!")


def main():
    soup = connect('https://www.drugs.com/drug_information.html')
    pages = soup.find('ul', {'class': 'ddc-paging'}).findAll('a')[1:]
    page_urls = [host + a['href'] for a in pages]

    page_tasks = []
    for page_url in page_urls:
        get_opinions_from_page.delay(page_url)


main()
